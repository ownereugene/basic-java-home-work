package com.hw9;

import java.util.Map;

public final class Man extends Human {
    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man() {
    }

    public Man(String name, String surname, long birthDate, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public void repairCar() {
        System.out.println("лагодити авто");
    }

    @Override
    public void greetPet() {
        System.out.println("Привіт як справи?, " + this.getFamily().getPet().getNickname());
    }
}
