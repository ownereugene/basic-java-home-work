package com.hw10.Services;

import com.hw10.*;
import com.hw10.DAO.CollectionFamilyDao;
import com.hw10.DAO.FamilyDao;

import java.time.Year;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FamilyService {
    private FamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = getAllFamilies();
        System.out.println("All families:");
        List<String> result = allFamilies.stream().map(family -> family.toString()).collect(Collectors.toList());
        IntStream.range(0, allFamilies.size()).forEach(index -> System.out.println(index + 1 + ". " + result.get(index)));
    }


    public List<Family> getFamiliesBiggerThan(int quantity) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() > quantity)
                .peek(System.out::println)
                .toList();
    }

    public List<Family> getFamiliesLessThan(int quantity) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() < quantity)
                .peek(System.out::println)
                .toList();
    }

    public int countFamiliesWithMemberNumber(int quantity) {
        return (int) getAllFamilies().stream()
                .filter(family -> family.countFamily() == quantity)
                .peek(System.out::println)
                .count();
    }


    public void createNewFamily(Human human1, Human human2) {
        Family family = new Family(human1, human2);
        familyDao.saveFamily(family);
    }


    public Family bornChild(Family family, String boyName, String girlName) {
        String childName = Math.random() < 0.5 ? boyName : girlName;
        Human child;
        if (childName.equals(boyName)) {
            child = new Man();
        } else {
            child = new Woman();
        }
        child.setFamily(family);
        child.setSurname(family.getFather().getSurname());
        child.setName(childName);
        family.addChild(child);
        return family;
    }

    public Family adoptChild(Family family, Human human) {
        family.addChild(human);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        Year currentYear = Year.now();
        getAllFamilies().stream()
                .forEach(family -> {family.setChildren(family.getChildren()
                        .stream().filter(human -> {
                            int birthYear = human.calculateBirthdayZonedDateTime(human.getBirthDate()).getYear();
                            int ageOfHuman = currentYear.getValue() - birthYear;
                            return ageOfHuman <= age;
                        }).toList()
                );
                    familyDao.saveFamily(family);
                });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Set<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        return family.getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        if (familyIndex >= 0 && familyIndex < familyDao.getAllFamilies().size()) {
            familyDao.getAllFamilies().get(familyIndex).addPetTwo(pet);
        }
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public boolean deleteFamilyByFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    public Family getFamilyById(int index) {
        System.out.println(familyDao.getFamilyByIndex(index));
        return familyDao.getFamilyByIndex(index);
    }
}