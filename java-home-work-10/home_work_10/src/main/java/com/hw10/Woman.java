package com.hw10;

import java.util.Map;

public final class Woman extends Human {
    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Woman() {
    }

    public Woman(String name, String surname, long birthDate, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public void makeup() {
        System.out.println("підфарбуватися");
    }

    @Override
    public void greetPet() {
        System.out.println("Привіт як справи?, " + this.getFamily().getPet().getNickname());
    }
}
