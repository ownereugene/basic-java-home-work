package com.hw12;

import java.text.ParseException;
import java.util.Map;

public final class Man extends Human {
    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man(String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule, Pet pet) {
        super(name, surname, birthDate, iq, schedule, pet);
    }

    public Man(String name, String surname, long birthDate, int iq, Map<DayOfWeek, String> schedule, String gender) {
        super(name, surname, birthDate, iq, schedule, gender);
    }

    public Man(String name, String surname, String birthDate, int iq) throws ParseException, ParseException {
        super(name, surname, birthDate, iq);
    }
    public Man() {
    }



    public void repairCar() {
        System.out.println("лагодити авто");
    }

    @Override
    public void greetPet() {
        System.out.println("Привіт як справи?, " + this.getFamily().getPet().getNickname());
    }
}
