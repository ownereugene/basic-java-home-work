package com.hw12.Services;

import com.hw12.*;
import com.hw12.DAO.CollectionFamilyDao;
import com.hw12.Exceptions.FamilyOverflowException;

import java.time.Year;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FamilyService {
    private CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
    private static final int MAX_FAMILY_SIZE = 10;

    public List<Family> getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        if (!this.getAllFamilies().isEmpty()) {
            List<Family> allFamilies = getAllFamilies();
            System.out.println("All families:");
            List<String> result = allFamilies.stream().map(family -> family.prettyFormat()).collect(Collectors.toList());
            IntStream.range(0, allFamilies.size())
                    .forEach(index -> System.out.println(index + 1 + ". " + result.get(index)));
        } else {
            System.out.println("No families");
        }

    }

    public List<Family> getFamiliesBiggerThan(int quantity) {
        AtomicInteger counter = new AtomicInteger(0);
        return collectionFamilyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > quantity)
                .peek(family -> {
                    int index = counter.incrementAndGet();
                    System.out.println(index + ". " + family.prettyFormat());
                })
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int quantity) {
        AtomicInteger counter = new AtomicInteger(0);
        return collectionFamilyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < quantity)
                .peek(family -> {
                    int index = counter.incrementAndGet();
                    System.out.println(index + ". " + family.prettyFormat());
                })
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int quantity) {
        AtomicInteger counter = new AtomicInteger(0);
        return (int) collectionFamilyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == quantity)
                .peek(family -> {
                    int index = counter.incrementAndGet();
                    System.out.println(index + ". " + family.prettyFormat());
                })
                .count();
    }

    public void createNewFamily(Human human1, Human human2) {
        Family family = new Family(human1, human2);
        collectionFamilyDao.saveFamily(family);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        if (family.countFamily() < MAX_FAMILY_SIZE) {
            String childName = Math.random() < 0.5 ? boyName : girlName;
            Human child;
            if (childName.equals(boyName)) {
                child = new Man();
            } else if (childName.equals(girlName)) {
                child = new Woman();
            } else {
                child = new Human();
            }
            child.setFamily(family);
            child.setSurname(family.getFather().getSurname());
            child.setName(childName);
            family.addChild(child);
            return family;
        } else {
            throw new FamilyOverflowException("Max limit");
        }
    }

    public Family adoptChild(Family family, Human human) {
        if (family.countFamily() < MAX_FAMILY_SIZE) {
            family.addChild(human);
            collectionFamilyDao.saveFamily(family);
            return family;
        } else {
            throw new FamilyOverflowException("Max limit");
        }
    }

    public void deleteAllChildrenOlderThen(int age) {
        Year currentYear = Year.now();
        getAllFamilies().stream()
                .forEach(family -> {
                    family.setChildren(family.getChildren()
                            .stream().filter(human -> {
                                int birthYear = human.calculateBirthdayZonedDateTime(human.getBirthDate()).getYear();
                                int ageOfHuman = currentYear.getValue() - birthYear;
                                return ageOfHuman <= age;
                            }).toList()
                    );
                    collectionFamilyDao.saveFamily(family);
                });
    }

    public int count() {
        System.out.println("The quantity of families is " + collectionFamilyDao.getAllFamilies().size());
        return getAllFamilies().size();
    }

    public Set<Pet> getPets(int index) {
        Family family = collectionFamilyDao.getFamilyByIndex(index);
        System.out.println("This family has pets: " + family.getPets());
        return family.getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        if (familyIndex >= 0 && familyIndex < getAllFamilies().size()) {
            getAllFamilies().get(familyIndex).addPet(pet);
        }
    }
    public void setAllFamilies(List<Family> familys) {
        collectionFamilyDao.setAllFamilies(familys);
    }


    public boolean deleteFamilyByIndex(int index) {
        return collectionFamilyDao.deleteFamily(index);
    }

    public boolean deleteFamilyByFamily(Family family) {
        return collectionFamilyDao.deleteFamily(family);
    }

    public Family getFamilyById(int index) {
        System.out.println(collectionFamilyDao.getFamilyByIndex(index));
        return collectionFamilyDao.getFamilyByIndex(index);
    }
}