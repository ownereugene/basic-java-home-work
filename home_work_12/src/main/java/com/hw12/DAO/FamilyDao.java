package com.hw12.DAO;

import com.hw12.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();
    void setAllFamilies(List<Family> families);
    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);


}
